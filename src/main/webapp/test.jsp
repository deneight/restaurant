<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/sql" prefix="sql" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>


<sql:query var="rs" dataSource="jdbc/restaurant">
    select category_name from categories
</sql:query>

<html>
<head>
    <title>DB Test</title>
</head>
<body>


<h2>Results</h2>

<c:forEach var="row" items="${rs.rows}">
    ${row.category_name}<br/>
</c:forEach>

</body>
</html>

