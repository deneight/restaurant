const app = (function () {
    let user = {};
    let locale = {};
    //basic forms
    function formRegistration() {
        return '<form class ="table" id="registration" name = "auth">' +
            '<p class="auth_label"><label for="login">' + locale['login'] + '</label></p>' +
            '<p class="auth_input"><input name="field" id ="login" ' +
            'placeholder="' + locale['loginInf'] + '" value=""></p>' +
            '<p class="auth_label"><label for="password">' + locale['password'] + '</label></p>' +
            '<p class="auth_input"><input type="password" name="field" id ="password" ' +
            'placeholder="' + locale['passwordInf'] + '" value=""></p>' +
            '<p class="auth_label"><label for="firstName">' + locale['firstName'] + '</label></p>' +
            '<p class="auth_input"><input name="field" id ="firstName" ' +
            'placeholder="' + locale['firstNameInf'] + '" value=""></p>' +
            '<p class="auth_label"><label for="lastName">' + locale['lastName'] + '</label></p>' +
            '<p class="auth_input"><input name="field" id ="lastName" ' +
            'placeholder="' + locale['lastNameInf'] + '" value=""></p>' +
            '<p><input class = "button" type="button" value="' + locale['signup'] + '"' +
            ' onclick = "app.validationUserData(form)">&nbsp&nbsp' +
            '<input class = "button" type="button" value="' + locale['signin'] + '"' +
            ' onclick = "app.getAuthorization(\'login\')"></p>'
    }

    function formAuthorization() {
        return '<form class ="table" id="authorization" name = "auth">' +
            '<p class="auth_label"><label for="login">' + locale['login'] + '</label></p>' +
            '<p class="auth_input"><input name="field" id ="login" ' +
            'placeholder="' + locale['loginInf'] + '" value=""></p>' +
            '<p class="auth_label"><label for="password">' + locale['password'] + '</label></p>' +
            '<p class="auth_input"><input type="password" name="field" id ="password" ' +
            'placeholder="' + locale['passwordInf'] + '" value=""></p>' +
            '<p><input class="button" type="button" value="' + locale['signin'] + '"' +
            ' onclick = "app.validationUserData(form)">&nbsp&nbsp' +
            '<input class="button" type="button" value="' + locale['signup'] + '"' +
            ' onclick = "app.getAuthorization(\'registration\')"></p>'
    }

    function formPublicMenu() {
        return '<ul class="main-menu">' +
            '<li><button class = "button" id="getLocale" onclick="app.ajax(\'AppLocale\',' +
            'app.getLocale,\'' + locale['changeLocale'] + '\');">' + locale['changeLocale'] + '</button></li>'
    }

    function formMenuForUsers() {
        let userLabel = locale[user['roleName']] + ": " + user["login"];
        return '<ul class="main-menu">' +
            '<li><button class = "button" id="getLocale" onclick="app.ajax(\'AppLocale\',' +
            'app.getLocale,\'' + locale['changeLocale'] + '\');">' + locale['changeLocale'] + '</button></li>' +
            '<div style="float:right;"><li>' + userLabel + '&nbsp' +
            '<button class = "button" id="currentUser" onclick="window.location.reload();">' +
            locale['exit'] + '</button></li></div>'
    }

    function formCategories(data) {
        return '<form class="table" id="categories' + data["categoryID"] + '" >' +
            '<input hidden name="categoryID" value="' + data["categoryID"] + '">' +
            '<input type="button" class = "button" value="' + data["categoryName"] +
            '" onclick = "app.ajax(\'GetItems\',app.getItemsList ,' +
            'form.elements[\'categoryID\'].value);"></form>'
    }

    function formItemList(data) {
        data["itemDescription"] = data["itemDescription"] || '';
        return '<form class="table" id="items' + data["itemID"] + '">' +
            '<input hidden name ="itemID" value="' + data["itemID"] + '">' +
            '<b>' + data["itemName"] + '</b><br>' + data["itemDescription"] +
            '</form>' + '<div class="table" id="itemsList' + data["itemID"] + '"></div>'
    }

    function formTypesList(data) {
        return '<form class="table" id="itemTypes' + data["itemTypeID"] + '">' +
            '<input hidden name ="itemTypeID" value="' + data["itemTypeID"] + '">' +
            ('roleName' in user && user['roleName'] === 'client'
                ? '<input type="button" class = "button" value="' + locale['addToCart'] + '" ' +
                'onclick = "app.ajax(\'GetOneType\',' +
                'app.getOneType, [form.elements[\'itemTypeID\'].value]);">&nbsp&nbsp'
                : '') +
            data["itemTypeName"] + ',&nbsp' + locale['price'] + ':&nbsp ' +
            data["itemTypePrice"] + '&nbsp' + locale['rubles'] + '&nbsp&nbsp' +
            '</form>'
    }

    function formOneType(data) {
        return '<form class="table" name="oneType">' +
            '<input  hidden name="itemTypeID" value="' + data["itemTypeID"] + '" ><b>' +
            data["itemName"] + '</b>&nbsp' + data["itemTypeName"] + '<br>' + locale['price'] + ':&nbsp' +
            data["itemTypePrice"] + '&nbsp' + locale['rubles'] + '<br>' + locale['quantity'] + ':<br>' +
            '<input name="itemTypesQuantity" type="number"  value="1" min = "1" max="99" id="number"><br><br>' +
            '<input type="button" class = "button" value="' + locale['delBasketItem'] + '"' +
            ' onclick = "this.parentNode.parentNode.remove();">' +
            '</form>'
    }


    function formOrder(data, buttonsForOrder) {
        let buttonData = getButtonsForOrdersManagement(data, buttonsForOrder),
            currentButton = (buttonData) ? formButtonsForOrderManagement(buttonData) : '',
            date = new Date(data["changeStatusDate"]).toLocaleString(locale['currentLocale']),
            orderSum = +data["orderSum"].toFixed(2);
        return '<form class="table" id="ordersList' + data["orderID"] + '">' +
            '<input hidden name="orderID"  value="' +
            data["orderID"] + '" ><b>' + locale['orderNumber'] + ':&nbsp' +
            data["orderID"] + '</b><br>' + locale['sum'] + ':&nbsp' +
            orderSum + '<br>' + locale['creationDateOrder'] + ':&nbsp' +
            date + '<br>' + locale['orderCurrentStatus'] + ':&nbsp' +
            locale[data['statusName']] + '<br>' + locale['orderWasPlaced'] + ':&nbsp' +
            data["firstName"] + '&nbsp' + data["lastName"] + '<br><br>' +
            '<input hidden id ="nextStatus" value="' + buttonData["nextStatusID"] + '">' +
            '<input type="button" class = "button" value="' + locale["logOrder"] + '"' +
            ' onclick = "app.ajax(\'GetLog\', app.getOrderInformation,' +
            'form.elements[\'orderID\'].value)">' +
            '&nbsp<input type="button" class = "button" value="' + locale['orderItems'] + '"' +
            ' onclick = "app.ajax(\'GetOrderItems\', app.getOrderInformation,' +
            'form.elements[\'orderID\'].value)">&nbsp' +
            currentButton + '</form>' +
            '<div id="information' + data["orderID"] + '"></div>';
    }

    function formOrderItems(data) {
        let itemTypesCost = +data["itemTypesCost"].toFixed(2);
        return '<form class="table" id="orderItems' + data["orderID"] + '" >' +
            '<input hidden name ="orderID" value="' +
            data["orderID"] + '">' + data["itemName"] + '&nbsp' +
            data["itemTypeName"] + '<br>' + locale['quantity'] + ':&nbsp' +
            data["itemTypesQuantity"] + '<br>' + locale['sum'] + ':&nbsp' +
            itemTypesCost + '&nbsp' + locale['rubles'] + '&nbsp&nbsp' +
            '</form>'
    }

    function formOrderLog(data) {
        let date = new Date(data["changeStatusDate"]).toLocaleString(locale['currentLocale']);
        return '<form class="table" id="orderLog' + data["orderID"] + '" >' +
            locale[data['statusName']] + '&nbsp' + data["firstName"] + '&nbsp' +
            data["lastName"] + '<br>' + date + '</form>'
    }
    //end basic forms

    //forms for buttons
    function formButtonsForOrderManagement(data) {
        return '<input type="button" class = "button" value="' +
            locale[data["nameOperation"]] + '"' +
            ' onclick = "app.ajax(\'EditOrderStatus&userID=' + user['userID'] + '\',' +
            'app.getOrders, [form.elements[\'orderID\'].value, form.elements[\'nextStatus\'].value])">'
    }

    function formButtonsForOrdersList() {
        return '<div class = "table" >' +
            '<button class = "button" id="getOrderForUser" onclick="' +
            'app.ajax(\'GetOrders&userID=' + user["userID"] + '\',' +
            'app.getOrders);">' + locale['refresh'] + '</button>&nbsp&nbsp' +
            locale['ordersMenuName'] + '</div>'
    }

    function formCloseInformationButton(path) {
        return '<input type="button" class = "button" value="' + locale['close'] + '"' +
            ' onclick = "app.clearSidebar(' + path + ');">'
    }

    function formButtonsForBasket() {
        return '<div class = "table" id="buttons_block">' +
            '<button class = "button" id="create_order" ' +
            'onclick="app.createOrder();">' + locale['addOrder'] + '</button>&nbsp' +
            '<button class = "button" id="clear" ' +
            'onclick="app.ajax(\'GetOrders&userID=' + user['userID'] + '\',' +
            'app.getOrders);">' + locale['close'] + '</button>&nbsp&nbsp' + locale["basket"] +
            '</div>'
    }

    function formButtonForItemsList() {
        return '<div class = "table" >' +
            '<button class = "button" id="clear" onclick="app.ajax(\'GetCategories\',' +
            'app.getCategoriesList);">' + locale['back'] + '</button>' +
            '</div>'
    }
    //end forms for buttons

    function getButtonsForOrdersManagement(data, buttonsForOrder) {
        if (buttonsForOrder) {
            for (let i = 0; i < buttonsForOrder.length; i++) {
                if (data["statusID"] === buttonsForOrder[i]['currentStatusID']) {
                    return buttonsForOrder[i];
                }
            }
        }
        return "";
    }

    function activationUserProfile(data) {
        if (data["status"] === "ok") {
            user = data["userData"];
            document.forms.auth.remove();
            app.launchMainPage(user['roleName']);
        }
        alert(data["message"]);
        //alert(JSON.stringify(data))
    }

    return {
        ajax: function (url, handler, data) {
            let response,
                xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
                    try {
                        response = JSON.parse(xmlhttp.responseText);
                    } catch (error) {
                        console.log(error.message + " in " + xmlhttp.responseText);
                        alert(error.message);
                        return;
                    }
                    handler(response);
                }
            };
            xmlhttp.open("POST", "frontController?command=" + url, true);
            (data) ? xmlhttp.send(JSON.stringify(data)) : xmlhttp.send();
        },

        //handlers for basic forms
        getCategoriesList: function (data) {
            let html = "";
            for (let i = 0; i < data["categoriesList"].length; i++) {
                html += formCategories(data["categoriesList"][i]);
            }
            document.getElementById('content').innerHTML = html;
        },

        getItemsList: function (data) {
            let html = formButtonForItemsList();
            for (let i = 0; i < data["itemsList"].length; i++) {
                html += formItemList(data["itemsList"][i]);
                app.ajax("GetItemTypes", app.getTypesList, data["itemsList"][i]["itemID"]);
            }
            document.getElementById("content").innerHTML = html;
        },

        getTypesList: function (data) {
            let html = "",
                itemID;
            for (let i = 0; i < data["typesList"].length; i++) {
                itemID = data["typesList"][i]["itemID"];
                html += formTypesList(data["typesList"][i]);
            }
            document.getElementById("itemsList" + itemID).innerHTML = html;
        },

        getOneType: function (data) {
            let itemTypes = document.querySelectorAll("input[name=itemTypeID]"),
                currentTypesQuantity;
            for (let currentType of itemTypes) {
                if (+currentType.value === data["oneType"][0]["itemTypeID"]) {
                    currentTypesQuantity = currentType.parentNode.children[5];
                }
            }
            if (!currentTypesQuantity) {
                app.clearSidebar('#orders_block');
                let newElement = document.createElement('basket'),
                    basketBlock = document.getElementById('basket_block'),
                    buttons = document.getElementById("buttons_block");
                if (!buttons) {
                    basketBlock.innerHTML = formButtonsForBasket();
                }
                newElement.className = "basket";
                newElement.innerHTML = formOneType(data["oneType"][0]);
                basketBlock.appendChild(newElement);
                newElement.outerHTML;
            } else {
                ++currentTypesQuantity.value;
            }
        },

        getOrders: function (data) {
            let html = formButtonsForOrdersList(),
                path = (user["roleName"] === 'client') ? 'orders_block' : 'content';
            if (path === 'orders_block') {
                app.clearSidebar('#basket_block');
            }
            if (data["ordersList"] !== 'null') {
                for (let i = 0; i < data["ordersList"].length; i++) {
                    html += formOrder(data["ordersList"][i], data['buttons']);
                }
            } else {
                html = '<div class="table">' + locale['emptyOrdersList'] + '</div>'
            }
            document.getElementById(path).innerHTML = html;
        },

        getOrderInformation: function (data) {
            let orderID, title,
                html = "";
            if (Object.keys(data).toString() === "orderItems") {
                title = locale['orderItems'];
                orderID = data["orderItems"][0]["orderID"];
                for (let i = 0; i < data["orderItems"].length; i++) {
                    html += formOrderItems(data["orderItems"][i]);
                }
            }
            if (Object.keys(data).toString() === "orderLog") {
                title = locale["logOrder"];
                orderID = data["orderLog"][0]["orderID"];
                for (let i = 0; i < data["orderLog"].length; i++) {
                    html += formOrderLog(data["orderLog"][i]);
                }
            }
            html += formCloseInformationButton('\'#information' + orderID + '\'');
            document.getElementById("information" + orderID).setAttribute('class', 'table');
            document.getElementById("information" + orderID).innerHTML =
                title + ' №' + orderID + '<br>' + html;
        },

        getAuthorization: function (type) {
            let elem = document.getElementById("auth_block");
            if (type === 'login') {
                elem.innerHTML = formAuthorization();
            } else {
                elem.innerHTML = formRegistration();
            }
        },
        //end handlers for basic forms

        createOrder: function () {
            let forms = document.getElementsByName('oneType'),
                orderItems = [];
            for (let i = 0; i < forms.length; i++) {
                let form = new FormData(forms[i]),
                    elementsItem = {};
                for (let pair of form.entries()) {
                    elementsItem[pair[0]] = pair[1];
                }
                orderItems.push(elementsItem);
            }
            console.log(JSON.stringify(orderItems));
            return app.ajax('AddOrder&userID=' + user["userID"], app.getOrders, orderItems);
        },

        clearSidebar: function (target) {
            let element = document.querySelector(target);
            element.removeAttribute('class');
            element.innerHTML = "";
        },

        validationUserData: function (form) {
            const REGEX_LOGIN = "^[a-zA-Z][a-zA-Z0-9-_\\.]{1,20}$",
                REGEX_PASSWORD = "(?=^.{8,}$)((?=.*\\d)|(?=.*\\W+))(?![.\\n])(?=.*[A-Z])(?=.*[a-z]).*$",
                REGEX_NAME = "^[а-яА-ЯёЁa-zA-Z]{1,20}$";
            let userData = {},
                path = null;
            for (let i = 0; i < form.elements.length; i++) {
                if (form.elements[i].name === "field") {
                    userData[form.elements[i].id] = form.elements[i].value;
                }
            }
            if (form.id === "authorization") {
                if (userData['login'].match(REGEX_LOGIN) && userData['password'].match(REGEX_PASSWORD)) {
                    path = "UserValidation";
                }
            }
            if (form.id === "registration") {
                if (userData['login'].match(REGEX_LOGIN) && userData['password'].match(REGEX_PASSWORD)
                    && userData['firstName'].match(REGEX_NAME) && userData['lastName'].match(REGEX_NAME)) {
                    path = "UserRegistration";
                }
            }
            if (path) {
                return app.ajax(path, activationUserProfile, userData);
            } else {
                alert("field is empty or wrong format: " + JSON.stringify(userData));
            }
        },

        launchMainPage: function (roleName) {
            let mainMenu;
            switch (roleName) {
                case undefined:
                    app.ajax('GetCategories', app.getCategoriesList);
                    app.getAuthorization('login');
                    mainMenu = formPublicMenu();
                    break;
                case "client":
                    app.ajax('GetCategories', app.getCategoriesList);
                    app.ajax('GetOrders&userID=' + user["userID"], app.getOrders);
                    mainMenu = formMenuForUsers();
                    break;
                case "admin":
                case "chef":
                    app.ajax('GetOrders&userID=' + user["userID"], app.getOrders);
                    mainMenu = formMenuForUsers();
                    break;
                default:
                    alert("non-existent role name")
            }
            document.getElementById("menu").innerHTML = mainMenu;
        },

        getLocale: function (data) {
            if (data) {
                locale = data;
                app.launchMainPage(user['roleName']);
            }
        }
    }
})();

(function () {
    app.ajax("AppLocale", app.getLocale, "ru");
})();
