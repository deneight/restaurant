package by.restaurant.entity;

import java.util.Objects;

/**
 * Entity class for data base table of orders_management.
 */
public class OrderManagement {
    private int currentStatusID;
    private int nextStatusID;
    private String nameOperation;
    private int roleID;

    public int getCurrentStatusID() {
        return currentStatusID;
    }

    public void setCurrentStatusID(int currentStatusID) {
        this.currentStatusID = currentStatusID;
    }

    public int getNextStatusID() {
        return nextStatusID;
    }

    public void setNextStatusID(int nextStatusID) {
        this.nextStatusID = nextStatusID;
    }

    public String getNameOperation() {
        return nameOperation;
    }

    public void setNameOperation(String nameOperation) {
        this.nameOperation = nameOperation;
    }

    public int getRoleID() {
        return roleID;
    }

    public void setRoleID(int roleID) {
        this.roleID = roleID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderManagement that = (OrderManagement) o;
        return currentStatusID == that.currentStatusID &&
                nextStatusID == that.nextStatusID &&
                roleID == that.roleID &&
                Objects.equals(nameOperation, that.nameOperation);
    }

    @Override
    public int hashCode() {

        return Objects.hash(currentStatusID, nextStatusID, nameOperation, roleID);
    }

    @Override
    public String toString() {
        return "OrderManagement{" +
                "currentStatusID=" + currentStatusID +
                ", nextStatusID=" + nextStatusID +
                ", nameOperation='" + nameOperation + '\'' +
                ", roleID=" + roleID +
                '}';
    }
}
