package by.restaurant.entity;

import lombok.Data;

/**
 * Entity class for data base table of categories.
 */
@Data
public class Category {
    private int categoryID;
    private String categoryName;
}
