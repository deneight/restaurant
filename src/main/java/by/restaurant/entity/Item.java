package by.restaurant.entity;

import java.util.Objects;

/**
 * Entity class for data base table of items.
 */
public class Item {
    private int itemID;
    protected String itemName;
    private String itemDescription;

    public int getItemID() {
        return itemID;
    }

    public void setItemID(int itemID) {
        this.itemID = itemID;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getItemDescription() {
        return itemDescription;
    }

    public void setItemDescription(String itemDescription) {
        this.itemDescription = itemDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Item item = (Item) o;
        return itemID == item.itemID &&
                Objects.equals(itemName, item.itemName) &&
                Objects.equals(itemDescription, item.itemDescription);
    }

    @Override
    public int hashCode() {

        return Objects.hash(itemID, itemName, itemDescription);
    }

    @Override
    public String toString() {
        return "Item{" +
                "itemID=" + itemID +
                ", itemName='" + itemName + '\'' +
                ", itemDescription='" + itemDescription + '\'' +
                '}';
    }
}
