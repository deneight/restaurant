package by.restaurant.entity;

import java.sql.Timestamp;
import java.util.Objects;

/**
 * Entity class for data base table of orders_logging.
 */
public class OrderInfo extends OrderStatus {
    protected int orderID;
    protected Timestamp changeStatusDate;
    protected int userID;
    protected String firstName;
    protected String lastName;

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public Timestamp getChangeStatusDate() {
        return changeStatusDate;
    }

    public void setChangeStatusDate(Timestamp changeStatusDate) {
        this.changeStatusDate = changeStatusDate;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderInfo orderInfo = (OrderInfo) o;
        return orderID == orderInfo.orderID &&
                userID == orderInfo.userID &&
                Objects.equals(changeStatusDate, orderInfo.changeStatusDate) &&
                Objects.equals(firstName, orderInfo.firstName) &&
                Objects.equals(lastName, orderInfo.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderID, changeStatusDate, userID, firstName, lastName);
    }

    @Override
    public String toString() {
        return "OrderInfo{" +
                "orderID=" + orderID +
                ", changeStatusDate=" + changeStatusDate +
                ", userID=" + userID +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", statusID=" + statusID +
                ", statusName='" + statusName + '\'' +
                '}';
    }
}
