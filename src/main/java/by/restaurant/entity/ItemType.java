package by.restaurant.entity;

import java.util.Objects;

/**
 * Entity class for data base table of item_types.
 */
public class ItemType extends Item {
    protected int itemTypeID;
    protected String itemTypeName;
    private double itemTypePrice;

    public int getItemTypeID() {
        return itemTypeID;
    }

    public void setItemTypeID(int itemTypeID) {
        this.itemTypeID = itemTypeID;
    }

    public String getItemTypeName() {
        return itemTypeName;
    }

    public void setItemTypeName(String itemTypeName) {
        this.itemTypeName = itemTypeName;
    }

    public double getItemTypePrice() {
        return itemTypePrice;
    }

    public void setItemTypePrice(double itemTypePrice) {
        this.itemTypePrice = itemTypePrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ItemType itemType = (ItemType) o;
        return itemTypeID == itemType.itemTypeID &&
                Double.compare(itemType.itemTypePrice, itemTypePrice) == 0 &&
                Objects.equals(itemName, itemType.itemName) &&
                Objects.equals(itemTypeName, itemType.itemTypeName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(itemTypeID, itemName, itemTypeName, itemTypePrice);
    }

    @Override
    public String toString() {
        return "ItemType{" +
                "itemTypeID=" + itemTypeID +
                ", itemTypeName='" + itemTypeName + '\'' +
                ", itemTypePrice=" + itemTypePrice +
                '}';
    }
}
