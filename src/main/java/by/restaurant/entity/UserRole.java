package by.restaurant.entity;

import java.util.Objects;

/**
 * Entity class for data base table of roles.
 */
public class UserRole {
    protected int roleID;
    protected String roleName;

    public UserRole(int roleID, String roleName) {
        this.roleID = roleID;
        this.roleName = roleName;
    }

    public UserRole() {
    }

    public int getRoleID() {
        return roleID;
    }

    public void setRoleID(int roleID) {
        this.roleID = roleID;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserRole userRole = (UserRole) o;
        return roleID == userRole.roleID &&
                Objects.equals(roleName, userRole.roleName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(roleID, roleName);
    }

    @Override
    public String toString() {
        return "UserRole{" +
                "roleID=" + roleID +
                ", roleName='" + roleName + '\'' +
                '}';
    }
}
