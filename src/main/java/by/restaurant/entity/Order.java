package by.restaurant.entity;

import java.util.Objects;

/**
 * Entity class for data base table of orders.
 */
public class Order extends OrderInfo {
    private double orderSum;

    public double getOrderSum() {
        return orderSum;
    }

    public void setOrderSum(double orderSum) {
        this.orderSum = orderSum;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Order order = (Order) o;
        return Double.compare(order.orderSum, orderSum) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), orderSum);
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderSum=" + orderSum +
                '}';
    }
}



