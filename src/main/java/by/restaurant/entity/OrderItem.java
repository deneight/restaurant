package by.restaurant.entity;

import java.util.Objects;

/**
 * Entity class for data base table of order_items.
 */
public class OrderItem extends ItemType {
    private int orderID;
    private int itemTypesQuantity;
    private double itemTypesCost;

    public OrderItem() {
    }

    public int getOrderID() {
        return orderID;
    }

    public void setOrderID(int orderID) {
        this.orderID = orderID;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getItemTypesQuantity() {
        return itemTypesQuantity;
    }

    public void setItemTypesQuantity(int itemTypesQuantity) {
        this.itemTypesQuantity = itemTypesQuantity;
    }

    public double getItemTypesCost() {
        return itemTypesCost;
    }

    public void setItemTypesCost(double itemTypesCost) {
        this.itemTypesCost = itemTypesCost;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OrderItem that = (OrderItem) o;
        return orderID == that.orderID &&
                itemTypesQuantity == that.itemTypesQuantity &&
                Double.compare(that.itemTypesCost, itemTypesCost) == 0 &&
                Objects.equals(itemName, that.itemName) &&
                Objects.equals(itemTypeName, that.itemTypeName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(orderID, itemName, itemTypeName, itemTypesQuantity, itemTypesCost);
    }

    @Override
    public String toString() {
        return "OrderItem{" +
                "orderID=" + orderID +
                ", itemTypesQuantity=" + itemTypesQuantity +
                ", itemTypesCost=" + itemTypesCost +
                '}';
    }
}
