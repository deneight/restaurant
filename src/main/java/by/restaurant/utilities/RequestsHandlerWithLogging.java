package by.restaurant.utilities;

import by.restaurant.entity.OrderItem;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * Handler for request from client side with logging. Read JSON and convert it to specific type.
 */
public class RequestsHandlerWithLogging {
    /**
     * Reads JSON and converts it to specific type.
     * @param type specific type for convertion
     * @param request Request from client
     * @param logger logger for current command where handls request
     * @param <T> Generic type
     * @return type which will be converted from JSON and it is current type in method parameter "Type"
     */
    public static <T> T readJSONData(Type type, HttpServletRequest request, Logger logger) { //TODO unsafe
        T dataFromJSON = null;
        try (JsonReader read = new JsonReader(new InputStreamReader(request.getInputStream(),
                StandardCharsets.UTF_8))) {
            dataFromJSON = new Gson().fromJson(read, type);
        } catch (IOException e) {
            logger.error(e);
        }
        return dataFromJSON;
    }
}
