package by.restaurant.utilities;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Objects;
import java.util.Properties;

/**
 * Provides the necessary configuration settings from properties files
 */
public class ConfigurationManager {
    private static ConfigurationManager instance = null;
    private static final String queriesSQL = "queriesSQL.properties";
    private static final Logger logger = LogManager.getLogger(ConfigurationManager.class);

    /**
     * Restricts the instantiation of a class to one object.
     *
     * @return object class ConfigurationManager only in one instance
     */
    public static ConfigurationManager getInstance() {
        if (instance == null)
            instance = new ConfigurationManager();
        return instance;
    }

    /**
     * Provides specified queries from property file.
     *
     * @param query key to query contained in property class object.
     * @return sql request.
     */
    public String getQuerySQL(String query) {
        return Objects.requireNonNull(getConfiguration(queriesSQL)).getProperty(query);
    }

    /**
     * Reads the specified properties files.
     *
     * @param file name of existing property file.
     * @return Property class object with data of configuration settings.
     */
    private Properties getConfiguration(String file) {
        Properties properties = null;

        try (InputStream read = ConfigurationManager.class.getResourceAsStream("/" + file)) {
            if (read == null) {
                logger.fatal("Unable to load property file: " + file);
            } else {
                properties = new Properties();
                try {
                    properties.load(read);
                } catch (IOException e) {
                    logger.fatal(e);
                }
            }
        } catch (IOException e) {
            logger.fatal(e);
        }
        return properties;
    }
}