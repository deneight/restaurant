package by.restaurant.connection;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.*;

/**
 * This class provides connection with data base.
 */
public class DataBaseConnection {
    private static final Logger logger = LogManager.getLogger(DataBaseConnection.class);
    private static DataBaseConnection instance = null;

    /**
     * Restricts the instantiation of a class to one object.
     *
     * @return object class DataBaseConnection only in one instance
     */
    public static DataBaseConnection getInstance() {
        if (instance == null) {
            instance = new DataBaseConnection();
        }
        return instance;
    }

    /**
     * Attempts to establish a connection with the data source
     * that this DataSource object represents.
     *
     * @return a connection to the data source.
     */
    public Connection getConnection() {
        Connection connection = null;

        try {
            Context context = new InitialContext();
            DataSource dataSource = (DataSource) context.lookup("java:comp/env/jdbc/restaurant");
            connection = dataSource.getConnection();
        } catch (NamingException | SQLException e) {
            logger.fatal(e);
        }
        return connection;
    }
}

