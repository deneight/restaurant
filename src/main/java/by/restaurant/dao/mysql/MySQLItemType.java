package by.restaurant.dao.mysql;

import by.restaurant.connection.DataBaseConnection;
import by.restaurant.dao.interfaces.DAOItemTypes;
import by.restaurant.entity.ItemType;
import by.restaurant.utilities.ConfigurationManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Provides to get data from a data source from item_types table.
 */
public class MySQLItemType implements DAOItemTypes {
    private static final Logger logger = LogManager.getLogger(MySQLItemType.class);

    //read by id item
    @Override
    public List<ItemType> getTypesByItem(int itemID) {
        List<ItemType> types = new ArrayList<>();

        try (Connection connection = DataBaseConnection.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(ConfigurationManager.getInstance()
                     .getQuerySQL("SELECT_TYPES_BY_ITEM"))) {

            preparedStatement.setInt(1, itemID);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    ItemType type = new ItemType();
                    type.setItemID(resultSet.getInt("id_item"));
                    type.setItemTypeID(resultSet.getInt("id_item_type"));
                    type.setItemTypeName(resultSet.getString("item_type_name"));
                    type.setItemTypePrice(resultSet.getDouble("item_type_price"));
                    types.add(type);
                }
                if (types.isEmpty()) {
                    logger.warn("data cannot be found in data base");
                }
            }
        } catch (SQLException e) {
            logger.error(e);
        }
        return types;
    }

    //read itemType by id for basket
    @Override
    public List<ItemType> getTypesData(int[] listTypeID) {
        List<ItemType> types = new ArrayList<>();

        StringBuilder additionalRequest = new StringBuilder("?");
        for (int i = 1; i < listTypeID.length; i++) {
            additionalRequest.append(" or id_item_type = ? ");
        }
        String changedQuery = ConfigurationManager.getInstance()
                .getQuerySQL("SELECT_ITEM_TYPE_BY_TYPE_ID")
                .replace("?", additionalRequest.toString());

        try (Connection connection = DataBaseConnection.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(changedQuery)) {

            int j = 1;
            for (int typeID : listTypeID) {
                preparedStatement.setInt(j++, typeID);
            }

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    ItemType type = new ItemType();
                    type.setItemTypeID(resultSet.getInt("id_item_type"));
                    type.setItemName(resultSet.getString("item_name"));
                    type.setItemTypeName(resultSet.getString("item_type_name"));
                    type.setItemTypePrice(resultSet.getDouble("item_type_price"));
                    types.add(type);
                }
                if (types.isEmpty()) {
                    logger.warn("data cannot be found in data base");
                }
            }
        } catch (SQLException e) {
            logger.error(e);
        }
        return types;
    }
}
