package by.restaurant.dao.mysql;

import by.restaurant.dao.interfaces.DAOItem;
import by.restaurant.entity.Item;
import by.restaurant.utilities.ConfigurationManager;
import by.restaurant.connection.DataBaseConnection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Provides to get data from a data source from items table.
 */
public class MySQLItem implements DAOItem {
    private static final Logger logger = LogManager.getLogger(MySQLItem.class);

    //read by id category
    @Override
    public List<Item> getALLItems(int categoryID) {
        List<Item> items = new ArrayList<>();

        try (Connection connection = DataBaseConnection.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(ConfigurationManager.getInstance()
                     .getQuerySQL("SELECT_ITEMS_BY_CATEGORY"))) {

            preparedStatement.setInt(1, categoryID);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    Item item = new Item();
                    item.setItemID(resultSet.getInt("id_item"));
                    item.setItemName(resultSet.getString("item_name"));
                    item.setItemDescription(resultSet.getString("item_description"));
                    items.add(item);
                }
                if (items.isEmpty()) {
                    logger.warn("data cannot be found in data base");
                }
            }
        } catch (SQLException e) {
            logger.error(e);
        }
        return items;
    }
}


