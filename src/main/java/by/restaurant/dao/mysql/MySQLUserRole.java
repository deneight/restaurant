package by.restaurant.dao.mysql;

import by.restaurant.connection.DataBaseConnection;
import by.restaurant.dao.interfaces.DAOUserRole;
import by.restaurant.entity.UserRole;
import by.restaurant.utilities.ConfigurationManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Provides to get data from a data source from roles table.
 */
public class MySQLUserRole implements DAOUserRole {
    private static final Logger logger = LogManager.getLogger(MySQLUserRole.class);

    //get userRole by id userRoleID
    @Override
    public UserRole getUserRole(int userRoleID) {
        UserRole userRole = null;

        try (Connection connection = DataBaseConnection.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(ConfigurationManager.getInstance()
                     .getQuerySQL("SELECT_USER_ROLE"))) {

            preparedStatement.setInt(1, userRoleID);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    userRole = new UserRole();
                    userRole.setRoleID(resultSet.getInt("id_role"));
                    userRole.setRoleName(resultSet.getString("role_name"));
                }
                if (userRole != null && userRole.getRoleID() == 0) {
                    logger.warn("data cannot be found in data base");
                }
            }
        } catch (SQLException e) {
            logger.error(e);
        }
        return userRole;
    }
}
