package by.restaurant.dao.mysql;

import by.restaurant.utilities.ConfigurationManager;
import by.restaurant.connection.DataBaseConnection;
import by.restaurant.dao.interfaces.DAOOrderInfo;
import by.restaurant.entity.OrderInfo;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Provides to get data from a data source from orders_logging table.
 */
public class MySQLOrderInfo implements DAOOrderInfo {
    private static final Logger logger = LogManager.getLogger(MySQLOrderInfo.class);

    //get all log
    @Override
    public List<OrderInfo> getLog(int orderID) {
        List<OrderInfo> log = new ArrayList<>();

        try (Connection connection = DataBaseConnection.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(ConfigurationManager.getInstance()
                     .getQuerySQL("SELECT_ORDER_LOG_BY_ORDER"))) {

            preparedStatement.setInt(1, orderID);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    OrderInfo note = new OrderInfo();
                    note.setOrderID(resultSet.getInt("id_order"));
                    note.setStatusName(resultSet.getString("status_name"));
                    note.setChangeStatusDate(resultSet.getTimestamp("status_changed"));
                    note.setFirstName(resultSet.getString("first_name"));
                    note.setLastName(resultSet.getString("last_name"));
                    log.add(note);
                }
                if (log.isEmpty()) {
                    logger.warn("data cannot be found in data base");
                }
            }
        } catch (SQLException e) {
            logger.error(e);
        }
        return log;
    }

    //add log
    @Override
    public void addLog(int orderID, int statusID, int userID) {
        try (Connection connection = DataBaseConnection.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(ConfigurationManager.getInstance()
                     .getQuerySQL("INSERT_LOG"))) {

            preparedStatement.setInt(1, orderID);
            preparedStatement.setInt(2, statusID);
            preparedStatement.setInt(3, userID);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            logger.error(e);
        }
    }
}
