package by.restaurant.dao.mysql;

import by.restaurant.dao.DAOFactory;
import by.restaurant.dao.interfaces.*;

/**
 * Provide  mysql factories for Data Access Object concrete classes.
 */
public class MySQLFactory extends DAOFactory {

    @Override
    public DAOCategory getCategoryDAO() {
        return new MySQLCategory();
    }

    @Override
    public DAOItem getItemDAO() {
        return new MySQLItem();
    }

    @Override
    public DAOItemTypes getItemTypesDAO() {
        return new MySQLItemType();
    }

    @Override
    public DAOOrderItems getOrderItemsDAO() {
        return new MySQLOrderItem();
    }

    @Override
    public DAOOrder getOrderDAO() {
        return new MySQLOrder();
    }

    @Override
    public DAOOrderInfo getOrderLoggingDAO() {
        return new MySQLOrderInfo();
    }

    @Override
    public DAOUser getUserDAO() {
        return new MySQLUser();
    }

    @Override
    public DAOOrderManagement getOrderManagementDAO() {
        return new MySQLOrderManagement();
    }

    @Override
    public DAOUserRole getUserRoleDAO() {
        return new MySQLUserRole();
    }

    @Override
    public DAOOrderStatus getOrderStatusDAO() {
        return new MySQLOrderStatus();
    }
}
