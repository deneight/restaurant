package by.restaurant.dao.mysql;

import by.restaurant.connection.DataBaseConnection;
import by.restaurant.dao.interfaces.DAOCategory;
import by.restaurant.entity.Category;
import by.restaurant.utilities.ConfigurationManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * Provides to get data from a data source from categories table.
 */
public class MySQLCategory implements DAOCategory {
    private static final Logger logger = LogManager.getLogger(MySQLCategory.class);

    //read all categories
    @Override
    public List<Category> getAllCategories() {
        List<Category> categories = new ArrayList<>();

        try (Connection connection = DataBaseConnection.getInstance().getConnection();
             Statement statement = connection.createStatement();
             ResultSet resultSet = statement.executeQuery(ConfigurationManager
                     .getInstance().getQuerySQL("SELECT_ALL_CATEGORIES"))) {

            while (resultSet.next()) {
                Category category = new Category();
                category.setCategoryID(resultSet.getInt("id_category"));
                category.setCategoryName(resultSet.getString("category_name"));
                categories.add(category);
            }
            if (categories.isEmpty()) {
                logger.warn("data cannot be found in data base");
            }
        } catch (SQLException e) {
            logger.error(e);
        }
        return categories;
    }
}

