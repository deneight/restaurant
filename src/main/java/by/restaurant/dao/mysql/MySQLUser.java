package by.restaurant.dao.mysql;

import by.restaurant.utilities.ConfigurationManager;
import by.restaurant.connection.DataBaseConnection;
import by.restaurant.dao.interfaces.DAOUser;
import by.restaurant.entity.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;

/**
 * Provides to get data from a data source from users table.
 */
public class MySQLUser implements DAOUser {
    private static final Logger logger = LogManager.getLogger(MySQLUser.class);

    //add user at registration with client role
    @Override
    public void addClient(User client) {
        try (Connection connection = DataBaseConnection.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(ConfigurationManager.getInstance()
                     .getQuerySQL("INSERT_CLIENT"))) {

            preparedStatement.setString(1, client.getLogin());
            preparedStatement.setString(2, client.getPassword());
            preparedStatement.setString(3, client.getFirstName());
            preparedStatement.setString(4, client.getLastName());
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            logger.error(e);
        }
    }

    //get user at validation
    @Override
    public User getUser(String login) {
        User user = null;

        try (Connection connection = DataBaseConnection.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(ConfigurationManager.getInstance()
                     .getQuerySQL("SELECT_USER_BY_LOGIN"))) {

            preparedStatement.setString(1, login);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    user = new User();
                    user.setUserID(resultSet.getInt("id_user"));
                    user.setLogin(resultSet.getString("login"));
                    user.setPassword(resultSet.getString("password"));
                    user.setRoleID(resultSet.getInt("id_role"));
                    user.setRoleName(resultSet.getString("role_name"));
                }
                if (user != null && user.getUserID() == 0) {
                    logger.info("data cannot be found in data base");
                }
            }
        } catch (SQLException e) {
            logger.error(e);
        }
        return user;
    }
}
