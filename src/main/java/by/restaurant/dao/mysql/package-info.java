/**
 * Provides classes DAO layer for CRUD operations for MySQL.
 */
package by.restaurant.dao.mysql;