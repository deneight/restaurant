package by.restaurant.dao.mysql;

import by.restaurant.utilities.ConfigurationManager;
import by.restaurant.connection.DataBaseConnection;
import by.restaurant.dao.interfaces.DAOOrder;
import by.restaurant.entity.Order;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Provides to get data from a data source from orders table.
 */
public class MySQLOrder implements DAOOrder {
    private static final Logger logger = LogManager.getLogger(MySQLOrder.class);

    @Override
    public List<Order> getOrdersByStatus(int[] listStatusID) {
        List<Order> orders = new ArrayList<>();

        StringBuilder additionalRequest = new StringBuilder("?");
        for (int i = 1; i < listStatusID.length; i++) {
            additionalRequest.append(" or orders.id_status = ? ");
        }
        String changedQuery = ConfigurationManager.getInstance().getQuerySQL("SELECT_ORDERS_FOR_EMPLOYEES")
                .replace("?", additionalRequest.toString());

        try (Connection connection = DataBaseConnection.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(changedQuery)) {
            int j = 1;
            for (int statusID : listStatusID) {
                preparedStatement.setInt(j++, statusID);
            }
            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                orders = readOrders(resultSet);
            }
        } catch (SQLException e) {
            logger.error(e);
        }
        return orders;
    }

    //get Order by id
    @Override
    public List<Order> getOrderForClient(int userID) {
        List<Order> orders = new ArrayList<>();

        try (Connection connection = DataBaseConnection.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(ConfigurationManager.getInstance()
                     .getQuerySQL("SELECT_ORDERS_FOR_CLIENT"))) {
            preparedStatement.setInt(1, userID);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                orders = readOrders(resultSet);
            }
        } catch (SQLException e) {
            logger.error(e);
        }
        return orders;
    }

    //add order and return its id
    @Override
    public Integer addOrder(double sum, int idUser) {
        int addedOrderID = 0;

        try (Connection connection = DataBaseConnection.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(ConfigurationManager.getInstance()
                     .getQuerySQL("INSERT_ORDER"), Statement.RETURN_GENERATED_KEYS)) {
            preparedStatement.setDouble(1, sum);
            preparedStatement.setInt(2, idUser);
            preparedStatement.executeUpdate();

            try (ResultSet resultSet = preparedStatement.getGeneratedKeys()) {
                resultSet.next();
                addedOrderID = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            logger.error(e);
        }
        return addedOrderID;
    }

    //date update, change status
    @Override
    public void changeStatus(int[] orderIDStatusID) {
        try (Connection connection = DataBaseConnection.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(ConfigurationManager.getInstance()
                     .getQuerySQL("UPDATE_ORDER"))) {
            preparedStatement.setInt(1, orderIDStatusID[1]);
            preparedStatement.setInt(2, orderIDStatusID[0]);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            logger.error(e);
        }
    }

    private List<Order> readOrders(ResultSet resultSet) {
        List<Order> orders = new ArrayList<>();

        try {
            while (resultSet.next()) {
                Order order = new Order();
                order.setOrderID(resultSet.getInt("id_order"));
                order.setOrderSum(resultSet.getDouble("order_sum"));
                order.setStatusID(resultSet.getInt("id_status"));
                order.setStatusName(resultSet.getString("status_name"));
                order.setChangeStatusDate(resultSet.getTimestamp("status_changed"));
                order.setFirstName(resultSet.getString("first_name"));
                order.setLastName(resultSet.getString("last_name"));
                orders.add(order);
            }
            if (orders.isEmpty()) {
                logger.info("data cannot be found in data base");
            }
        } catch (SQLException e) {
            logger.error(e);
        }
        return orders;
    }
}


