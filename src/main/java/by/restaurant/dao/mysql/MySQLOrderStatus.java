package by.restaurant.dao.mysql;

import by.restaurant.connection.DataBaseConnection;
import by.restaurant.dao.interfaces.DAOOrderStatus;
import by.restaurant.entity.OrderStatus;
import by.restaurant.utilities.ConfigurationManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Provides to get data from a data source from order_statuses table.
 */
public class MySQLOrderStatus implements DAOOrderStatus {
    private static final Logger logger = LogManager.getLogger(MySQLOrderStatus.class);

    //get orderStatus by statusName
    @Override
    public OrderStatus getOrderStatus(String statusName) {
        OrderStatus orderStatus = null;

        try (Connection connection = DataBaseConnection.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(ConfigurationManager.getInstance()
                     .getQuerySQL("SELECT_ORDER_STATUS_BY_NAME"))) {

            preparedStatement.setString(1, statusName);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    orderStatus = new OrderStatus();
                    orderStatus.setStatusID(resultSet.getInt("id_status"));
                    orderStatus.setStatusName(resultSet.getString("status_name"));
                }
                if (orderStatus != null && orderStatus.getStatusID() == 0) {
                    logger.warn("data cannot be found in data base");
                }
            }
        } catch (SQLException e) {
            logger.error(e);
        }
        return orderStatus;
    }
}
