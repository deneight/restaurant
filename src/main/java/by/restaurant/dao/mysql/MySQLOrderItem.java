package by.restaurant.dao.mysql;

import by.restaurant.connection.DataBaseConnection;
import by.restaurant.dao.interfaces.DAOOrderItems;
import by.restaurant.entity.OrderItem;
import by.restaurant.utilities.ConfigurationManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Provides to get data from a data source from order_items table.
 */
public class MySQLOrderItem implements DAOOrderItems {
    private static final Logger logger = LogManager.getLogger(MySQLOrderItem.class);

    //read order items by order id
    @Override
    public List<OrderItem> getOrderItems(int orderID) {
        List<OrderItem> orderItems = new ArrayList<>();

        try (Connection connection = DataBaseConnection.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(ConfigurationManager.getInstance()
                     .getQuerySQL("SELECT_ORDER_ITEMS_BY_ORDER"))) {

            preparedStatement.setInt(1, orderID);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    OrderItem orderItem = new OrderItem();
                    orderItem.setOrderID(resultSet.getInt("id_order"));
                    orderItem.setItemName(resultSet.getString("item_name"));
                    orderItem.setItemTypeName(resultSet.getString("item_type_name"));
                    orderItem.setItemTypesQuantity(resultSet.getInt("items_quantity"));
                    orderItem.setItemTypesCost(resultSet.getDouble("cost"));
                    orderItems.add(orderItem);
                }
                if (orderItems.isEmpty()) {
                    logger.warn("data cannot be found in data base");
                }
            }
        } catch (SQLException e) {
            logger.error(e);
        }
        return orderItems;
    }

    // add item type to the current order
    @Override
    public void addOrderItem(int orderID, List<OrderItem> orderTypes) {
        try (Connection connection = DataBaseConnection.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(ConfigurationManager.getInstance()
                     .getQuerySQL("INSERT_ORDER_ITEM"))) {

            for (OrderItem orderType : orderTypes) {
                preparedStatement.setInt(1, orderID);
                preparedStatement.setInt(2, orderType.getItemTypeID());
                preparedStatement.setInt(3, orderType.getItemTypesQuantity());
                preparedStatement.addBatch();
            }
            preparedStatement.executeBatch();

        } catch (SQLException e) {
            logger.error(e);
        }
    }

}
