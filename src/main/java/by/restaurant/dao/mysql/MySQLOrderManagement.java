package by.restaurant.dao.mysql;

import by.restaurant.connection.DataBaseConnection;
import by.restaurant.dao.interfaces.DAOOrderManagement;
import by.restaurant.entity.OrderManagement;
import by.restaurant.utilities.ConfigurationManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Provides to get data from a data source from orders_management table.
 */
public class MySQLOrderManagement implements DAOOrderManagement {
    private static final Logger logger = LogManager.getLogger(MySQLOrderManagement.class);

    //get all operations by user id
    @Override
    public List<OrderManagement> getOperations(int userID) {
        List<OrderManagement> operations = new ArrayList<>();

        try (Connection connection = DataBaseConnection.getInstance().getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(ConfigurationManager.getInstance()
                     .getQuerySQL("SELECT_OPERATIONS_BY_USER"))) {

            preparedStatement.setInt(1, userID);

            try (ResultSet resultSet = preparedStatement.executeQuery()) {
                while (resultSet.next()) {
                    OrderManagement operation = new OrderManagement();
                    operation.setCurrentStatusID(resultSet.getInt("id_current_status"));
                    operation.setNextStatusID(resultSet.getInt("id_next_status"));
                    operation.setNameOperation(resultSet.getString("name_operation"));
                    operation.setRoleID(resultSet.getInt("id_role"));
                    operations.add(operation);
                }
                if (operations.isEmpty()) {
                    logger.warn("data cannot be found in data base");
                }
            }
        } catch (SQLException e) {
            logger.error(e);
        }
        return operations;
    }
}
