package by.restaurant.dao;

import by.restaurant.dao.interfaces.*;
import by.restaurant.dao.mysql.MySQLFactory;

/**
 * Provides the necessary factory for specified type of data base
 * and defines which constructors to be performed
 * for Data Access Object concrete class.
 */
public abstract class DAOFactory {

    /**
     * Contains the list of supported databases.
     */
    public enum Factories {
        MySQL
    }

    /**
     * Provides the necessary factory for specified type of data base.
     * @param factoryName factory name contains in enum {@link Factories}.
     * @return specified type of data base factory.
     */
    public static DAOFactory getDAOFactory(Factories factoryName) {
        switch (factoryName) {
            case MySQL:
                return new MySQLFactory();
            default:
                return null;
        }
    }

    /**
     * Provides DAO for Category.
     * @return DAO Category.
     */
    public abstract DAOCategory getCategoryDAO();

    /**
     * Provides DAO for Item.
     * @return DAO Item.
     */
    public abstract DAOItem getItemDAO();

    /**
     * Provides DAO for ItemType.
     * @return DAO ItemType.
     */
    public abstract DAOItemTypes getItemTypesDAO();

    /**
     * Provides DAO for OrderItem.
     * @return DAO OrderItem.
     */
    public abstract DAOOrderItems getOrderItemsDAO();

    /**
     * Provides DAO for Order.
     * @return DAO Order.
     */
    public abstract DAOOrder getOrderDAO();

    /**
     * Provides DAO for OrderLogging.
     * @return DAO OrderLogging.
     */
    public abstract DAOOrderInfo getOrderLoggingDAO();

    /**
     * Provides DAO for User.
     * @return DAO User.
     */
    public abstract DAOUser getUserDAO();

    /**
     * Provides DAO for OrderManagement.
     * @return DAO OrderManagement.
     */
    public abstract DAOOrderManagement getOrderManagementDAO();

    /**
     * Provides DAO for UserRole.
     * @return DAO UserRole.
     */
    public abstract DAOUserRole getUserRoleDAO();

    /**
     * Provides DAO for OrderStatus.
     * @return DAO OrderStatus.
     */
    public abstract DAOOrderStatus getOrderStatusDAO();

}
