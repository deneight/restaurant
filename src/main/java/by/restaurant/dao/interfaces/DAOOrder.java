package by.restaurant.dao.interfaces;

import by.restaurant.entity.Order;

import java.util.List;

/**
 * Defines the standard operations to be performed on {@link Order}.
 */
public interface DAOOrder {

    /**
     * Reads orders by list of statuses id for employees.
     *
     * @param statusID list of statuses id.
     * @return list of orders for employees.
     */
    List<Order> getOrdersByStatus(int[] statusID);

    /**
     * Reads orders by user id for client.
     *
     * @param userID user id.
     * @return list of orders for client.
     */
    List<Order> getOrderForClient(int userID);

    /**
     * Creates new order.
     *
     * @param orderSum order items sum.
     * @param userID   user id.
     * @return new order id
     */
    Integer addOrder(double orderSum, int userID);

    /**
     * Updates order status and date of this change.
     *
     * @param newStatusID new status id.
     */
    void changeStatus(int[] newStatusID);
}
