package by.restaurant.dao.interfaces;

import by.restaurant.entity.OrderStatus;

/**
 * Defines the standard operations to be performed on {@link OrderStatus}.
 */
public interface DAOOrderStatus {

    /**
     * Reads order status by status name.
     *
     * @param statusName status name.
     * @return status data.
     */
    OrderStatus getOrderStatus(String statusName);
}
