package by.restaurant.dao.interfaces;

import by.restaurant.entity.Item;

import java.util.List;

/**
 * Defines the standard operations to be performed on {@link Item}.
 */
public interface DAOItem {
    /**
     * Reads items by category id.
     *
     * @param categoryID category id.
     * @return list of items.
     */
    List<Item> getALLItems(int categoryID);
}
