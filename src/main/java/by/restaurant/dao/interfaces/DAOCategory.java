package by.restaurant.dao.interfaces;

import by.restaurant.entity.Category;

import java.util.List;

/**
 * Defines the standard operations to be performed on {@link Category}.
 */
public interface DAOCategory {
    /**
     * Reads all categories.
     *
     * @return list of categories.
     */
    List<Category> getAllCategories();
}
