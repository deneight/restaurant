package by.restaurant.dao.interfaces;

import by.restaurant.entity.UserRole;

/**
 * Defines the standard operations to be performed on {@link UserRole}.
 */
public interface DAOUserRole {

    /**
     * Reads user role data by role id.
     *
     * @param userRoleID user role id.
     * @return role data.
     */
    UserRole getUserRole(int userRoleID);
}
