package by.restaurant.dao.interfaces;

import by.restaurant.entity.OrderInfo;

import java.util.List;

/**
 * Defines the standard operations to be performed on {@link OrderInfo}.
 */
public interface DAOOrderInfo {

    /**
     * Reads order logging by order id.
     *
     * @param orderID order id.
     * @return list of logs for one order.
     */
    List<OrderInfo> getLog(int orderID);

    /**
     * Creates log for order at operation with it.
     *
     * @param idOrder  order id.
     * @param idStatus order status id.
     * @param idUser   user id, who makes operation.
     */
    void addLog(int idOrder, int idStatus, int idUser);
}
