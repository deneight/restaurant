package by.restaurant.dao.interfaces;

import by.restaurant.entity.ItemType;

import java.util.List;

/**
 * Defines the standard operations to be performed on {@link ItemType}.
 */
public interface DAOItemTypes {
    /**
     * Reads item types by item id.
     *
     * @param itemID item id.
     * @return list of types.
     */
    List<ItemType> getTypesByItem(int itemID);

    /**
     * Reads one item type by type id for basket on client-side.
     *
     * @param itemTypeID array of item types id.
     * @return list of item types.
     */
    List<ItemType> getTypesData(int[] itemTypeID);

}
