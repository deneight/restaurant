package by.restaurant.dao.interfaces;

import by.restaurant.entity.OrderItem;

import java.util.List;

/**
 * Defines the standard operations to be performed on {@link OrderItem}.
 */
public interface DAOOrderItems {

    /**
     * Reads order items by order id.
     *
     * @param orderID order id.
     * @return list of items order.
     */
    List<OrderItem> getOrderItems(int orderID);

    /**
     * Creates order items to the order.
     *
     * @param orderID    order id.
     * @param orderTypes list of objects with data of item quantity
     *                   and item type id.
     */
    void addOrderItem(int orderID, List<OrderItem> orderTypes);
}
