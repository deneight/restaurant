package by.restaurant.dao.interfaces;

import by.restaurant.entity.User;

/**
 * Defines the standard operations to be performed on {@link User}.
 */
public interface DAOUser {

    /**
     * Creates new user with role of client at registration.
     *
     * @param user new user data.
     */
    void addClient(User user);

    /**
     * Reads user by login for validation.
     *
     * @param login user login.
     * @return user data.
     */
    User getUser(String login);
}
