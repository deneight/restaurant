package by.restaurant.dao.interfaces;

import by.restaurant.entity.OrderManagement;

import java.util.List;

/**
 * Defines the standard operations to be performed on {@link OrderManagement}.
 */
public interface DAOOrderManagement {

    /**
     * Reads order operations available for user.
     *
     * @param userID user id.
     * @return list of available operations for user.
     */
    List<OrderManagement> getOperations(int userID);
}
