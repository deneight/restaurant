package by.restaurant.controller;

import by.restaurant.controller.commands.FrontCommand;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Objects;

/**
 * Dispatches requests to commands in order
 * to trigger behavior associated with a request.
 */
public class FrontControllerServlet extends HttpServlet {
    private static final String PATH_TO_COMMAND = "by.restaurant.controller.commands.%s";
    private static final Logger logger = LogManager.getLogger(FrontControllerServlet.class);

    /**
     * Called by the server (via the service method)
     * to allow a servlet to handle a POST request.
     * Contains method doGet() and calls it.
     *
     * @param request  the HttpServletRequest object.
     * @param response the HttpServletResponse object.
     * @throws ServletException Defines a general exception
     *                          a servlet can throw when it encounters difficulty.
     * @throws IOException      Signals that an I/O exception of some sort has occurred.
     *                          This class is the general class of exceptions produced by failed or
     *                          interrupted I/O operations.
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    /**
     * Called by the server (via the service method)
     * to allow a servlet to handle a GET request.
     * Initializes a servlet and executes command.
     *
     * @param request  the HttpServletRequest object.
     * @param response the HttpServletResponse object.
     * @throws ServletException Defines a general exception
     *                          a servlet can throw when it encounters difficulty.
     * @throws IOException      Signals that an I/O exception of some sort has occurred.
     *                          This class is the general class of exceptions produced by failed or
     *                          interrupted I/O operations.
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        FrontCommand command = getCommand(request);
        Objects.requireNonNull(command).init(getServletContext(), request, response);
        try {
            command.action();
        } catch (SQLException e) {
            logger.fatal(e);
        }
    }

    /**
     * Extracts the command name from a request,
     * creating dynamically a new instance of a command class.
     *
     * @param request the HttpServletRequest object.
     * @return a new instance of a command class.
     */
    private FrontCommand getCommand(HttpServletRequest request) {
        try {
            Class type = Class.forName(String.format(PATH_TO_COMMAND, request.getParameter("command")));
            return (FrontCommand) type
                    .asSubclass(FrontCommand.class)
                    .newInstance();
        } catch (Exception e) {
            logger.fatal(e);
        }
        return null;
    }
}
