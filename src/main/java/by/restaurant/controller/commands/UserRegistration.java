package by.restaurant.controller.commands;

import by.restaurant.dao.DAOFactory;
import by.restaurant.dao.DAOFactory.Factories;
import by.restaurant.dao.interfaces.DAOUser;
import by.restaurant.entity.User;

import by.restaurant.utilities.RequestsHandlerWithLogging;
import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Objects;

/**
 * Receives user data from the client at registration,
 * checks the login (the match to the existing one in the database),
 * transfers user data to the database and
 * sends {@link by.restaurant.entity.User} data object in format json
 * or message that this login already exists.
 * Overrides {@link FrontCommand#action()}.
 */
public class UserRegistration extends FrontCommand {
    private static final String MESSAGE_AND_NEW_USER_DATA =
            "{\"message\": \"welcome\",\"status\":\"ok\", \"userData\": %s}";
    private static final String MESSAGE_EXISTED_LOGIN =
            "{\"message\":\"this login already exists\", \"status\":\"error\"}";
    private static final Logger logger = LogManager.getLogger(UserRegistration.class);

    @Override
    public void action() {
        DAOUser userDAO = Objects.requireNonNull(DAOFactory.getDAOFactory(Factories.MySQL)).getUserDAO();

        //read data user at registration
        User newUser = RequestsHandlerWithLogging.readJSONData(User.class, request, logger);
        //match login to the existing one in the database
        User userWithIdenticalLogin = userDAO.getUser(Objects.requireNonNull(newUser).getLogin());
        //add new user and send his data
        if (userWithIdenticalLogin == null) {
            userDAO.addClient(newUser);
            responseJson(String.format(MESSAGE_AND_NEW_USER_DATA,
                    new Gson().toJson(userDAO.getUser(newUser.getLogin()))));
        } else {
            //if has user with identical login
            responseJson(MESSAGE_EXISTED_LOGIN);
        }
    }
}
