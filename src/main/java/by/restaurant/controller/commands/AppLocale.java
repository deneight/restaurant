package by.restaurant.controller.commands;

import by.restaurant.utilities.RequestsHandlerWithLogging;
import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

/**
 * Receives locale data from the client
 * defines the necessary locale and reads its data in resource file,
 * then sends this locale data in format json.
 * Overrides {@link FrontCommand#action()}.
 */
public class AppLocale extends FrontCommand {
    private static final Logger logger = LogManager.getLogger(AppLocale.class);

    @Override
    public void action() {
        String language = RequestsHandlerWithLogging.readJSONData(String.class, request, logger);

        ResourceBundle bundle = null;
        if (Objects.requireNonNull(language).equals("en")) {
            bundle = ResourceBundle.getBundle("resources", new Locale("en", "US"));
        }
        if (Objects.requireNonNull(language).equals("ru")) {
            bundle = ResourceBundle.getBundle("resources", new Locale("ru", "RU"));
        }

        Map<String, String> locale = new HashMap<>();
        for (String key : Objects.requireNonNull(bundle).keySet()) {
            locale.put(key, bundle.getString(key));
        }

        responseJson(new Gson().toJson(locale));
    }
}
