package by.restaurant.controller.commands;

import by.restaurant.dao.DAOFactory;
import by.restaurant.dao.DAOFactory.Factories;
import by.restaurant.utilities.RequestsHandlerWithLogging;
import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Objects;

/**
 * Receives type id from the client
 * and sends {@link by.restaurant.entity.ItemType} object
 * a this type in format json.
 * Overrides {@link FrontCommand#action()}.
 */
public class GetOneType extends FrontCommand {
    private static final String NAME_ONE_TYPE_DATA = "{\"oneType\": %s }";
    private static final Logger logger = LogManager.getLogger(GetOneType.class);

    @Override
    public void action() {
        int[] listTypeID = RequestsHandlerWithLogging.readJSONData(int[].class, request, logger);

        responseJson(String.format(NAME_ONE_TYPE_DATA, new Gson().toJson(Objects.requireNonNull(DAOFactory
                .getDAOFactory(Factories.MySQL))
                .getItemTypesDAO()
                .getTypesData(listTypeID))));
    }
}
