package by.restaurant.controller.commands;

import by.restaurant.dao.DAOFactory;
import by.restaurant.dao.DAOFactory.Factories;
import by.restaurant.utilities.RequestsHandlerWithLogging;
import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Objects;

/**
 * Receives category id from the client
 * and sends list of {@link by.restaurant.entity.Item} objects
 * a this category in format json.
 * Overrides {@link FrontCommand#action()}.
 */
public class GetItems extends FrontCommand {
    private static final String NAME_ITEMS_LIST = "{\"itemsList\": %s }";
    private static final Logger logger = LogManager.getLogger(GetItems.class);

    @Override
    public void action() {
        int categoryID = RequestsHandlerWithLogging.readJSONData(int.class, request, logger);

        responseJson(String.format(NAME_ITEMS_LIST, new Gson().toJson(Objects.requireNonNull(DAOFactory
                .getDAOFactory(Factories.MySQL))
                .getItemDAO()
                .getALLItems(categoryID))));
    }
}
