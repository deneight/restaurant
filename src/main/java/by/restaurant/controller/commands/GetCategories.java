package by.restaurant.controller.commands;

import by.restaurant.dao.DAOFactory;
import by.restaurant.dao.DAOFactory.Factories;

import com.google.gson.Gson;

import java.util.*;

/**
 * Sends list of {@link by.restaurant.entity.Category} objects
 * in format json to the client.
 * Overrides {@link FrontCommand#action()}.
 */
public class GetCategories extends FrontCommand {
    final static private String NAME_CATEGORIES_LIST = "{\"categoriesList\": %s }";

    @Override
    public void action() {
        responseJson(String.format(NAME_CATEGORIES_LIST, new Gson().toJson(Objects.requireNonNull(DAOFactory
                .getDAOFactory(Factories.MySQL))
                .getCategoryDAO()
                .getAllCategories())));
    }
}
