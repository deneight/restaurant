package by.restaurant.controller.commands;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/**
 * Abstract class, which is holding the behavior common to all commands.
 * This class has access to the ServletContext and its request and response objects.
 * Contains common methods for all commands: responseJson(), forward().
 */
public abstract class FrontCommand {
    final static private String URL = "/frontController?command=%s";
    private static final Logger logger = LogManager.getLogger(FrontCommand.class);

    protected ServletContext context;
    protected HttpServletRequest request;
    protected HttpServletResponse response;

    /**
     * Perform servlet initialization - creating objects that are used by
     * the servlet in the handling of its requests and gets the context.
     *
     * @param servletContext  ServletContext object that gets the context from
     *                        the servlet's ServletConfig object.
     * @param servletRequest  the HttpServletRequest object.
     * @param servletResponse the HttpServletResponse object.
     */
    public void init(
            ServletContext servletContext,
            HttpServletRequest servletRequest,
            HttpServletResponse servletResponse) {

        this.context = servletContext;
        this.request = servletRequest;
        this.response = servletResponse;
    }

    /**
     * Defines the standard operation to be performed on all commands.
     *
     * @throws ServletException Defines a general exception a servlet can throw when it
     *                          encounters difficulty.
     * @throws IOException      Signals that an I/O exception of some sort has occurred. This
     *                          class is the general class of exceptions produced by failed or
     *                          interrupted I/O operations.
     * @throws SQLException     An exception that provides information on a database access
     *                          error or other errors.
     */
    public abstract void action() throws ServletException, IOException, SQLException;

    /**
     * Sends data to the client
     * in the format "UTF-8" and "application/json".
     *
     * @param json data in format json, that sends to the client.
     */
    protected void responseJson(String json) {
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        try {
            response.getWriter().write(json);
        } catch (IOException e) {
            logger.error(e);
        }
    }

    /**
     * Allows servlet to do preliminary processing of
     * a request and another resource to generate
     * the response.
     *
     * @param target a String specifying the pathname
     *               to the resource.
     */
    protected void forward(String target) { //TODO Reassigned parameter
        target = String.format(URL, target);
        RequestDispatcher dispatcher = context.getRequestDispatcher(target);
        try {
            dispatcher.forward(request, response);
        } catch (ServletException | IOException e) {
            logger.error(e);
        }
    }
}
