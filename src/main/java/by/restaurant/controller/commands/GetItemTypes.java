package by.restaurant.controller.commands;

import by.restaurant.dao.DAOFactory;
import by.restaurant.dao.DAOFactory.Factories;
import by.restaurant.utilities.RequestsHandlerWithLogging;
import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Objects;

/**
 * Receives item id from the client
 * and sends list of {@link by.restaurant.entity.ItemType} objects
 * a this item in format json.
 * Overrides {@link FrontCommand#action()}.
 */
public class GetItemTypes extends FrontCommand {
    private static final String NAME_TYPES_LIST = "{\"typesList\": %s}";
    private static final Logger logger = LogManager.getLogger(GetItemTypes.class);

    @Override
    public void action() {
        int itemID = RequestsHandlerWithLogging.readJSONData(int.class, request, logger);

        responseJson(String.format(NAME_TYPES_LIST, new Gson().toJson(Objects.requireNonNull(DAOFactory
                .getDAOFactory(Factories.MySQL))
                .getItemTypesDAO()
                .getTypesByItem(itemID))));
    }
}
