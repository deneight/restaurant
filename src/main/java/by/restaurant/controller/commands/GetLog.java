package by.restaurant.controller.commands;

import by.restaurant.dao.DAOFactory;
import by.restaurant.dao.DAOFactory.Factories;
import by.restaurant.utilities.RequestsHandlerWithLogging;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Objects;

/**
 * Receives order id from the client
 * and sends list of {@link by.restaurant.entity.OrderInfo} objects
 * this order in format json.
 * Overrides {@link FrontCommand#action()}.
 */
public class GetLog extends FrontCommand {
    private static final String NAME_ORDER_LOG = "{\"orderLog\": %s }";
    private static final Logger logger = LogManager.getLogger(GetLog.class);

    @Override
    public void action() {
        int orderID = RequestsHandlerWithLogging.readJSONData(int.class, request, logger);

        responseJson(String.format(NAME_ORDER_LOG, new Gson().toJson(Objects.requireNonNull(DAOFactory
                .getDAOFactory(Factories.MySQL))
                .getOrderLoggingDAO()
                .getLog(orderID))));
    }
}
