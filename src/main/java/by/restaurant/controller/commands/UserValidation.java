package by.restaurant.controller.commands;

import by.restaurant.dao.DAOFactory;
import by.restaurant.dao.DAOFactory.Factories;
import by.restaurant.dao.interfaces.DAOUser;
import by.restaurant.entity.User;

import by.restaurant.utilities.RequestsHandlerWithLogging;
import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Objects;

/**
 * Receives user data from the client at validation,
 * checks the login and password (the match to the existing one in the database)
 * sends {@link by.restaurant.entity.User} data object in format json
 * or messages: wrong login, wrong password.
 * Overrides {@link FrontCommand#action()}.
 */
public class UserValidation extends FrontCommand {
    private static final String MESSAGE_WRONG_LOGIN =
            "{\"message\": \"wrong login\",\"status\":\"error\"}";
    private static final String MESSAGE_WRONG_PASSWORD =
            "{\"message\": \"wrong password\",\"status\":\"error\"}";
    private static final String MESSAGE_AND_DATA_EXISTING_USER =
            "{\"message\": \"welcome\",\"status\":\"ok\", \"userData\": %s}";
    private static final Logger logger = LogManager.getLogger(UserValidation.class);

    @Override
    public void action() {
        DAOUser userDAO = Objects.requireNonNull(DAOFactory.getDAOFactory(Factories.MySQL)).getUserDAO();

        //read data user at validation
        User loginAndPassword = RequestsHandlerWithLogging.readJSONData(User.class, request, logger);
        //match login to the existing one in the database
        User existingUser = userDAO.getUser(Objects.requireNonNull(loginAndPassword).getLogin());
        if (existingUser == null) {
            responseJson(MESSAGE_WRONG_LOGIN);
        } else {
            //match password to the existing one in the database
            if (!existingUser.getPassword().equals(loginAndPassword.getPassword())) {
                responseJson(MESSAGE_WRONG_PASSWORD);
            } else {
                //TODO there was user-constructor here
                responseJson(String.format(MESSAGE_AND_DATA_EXISTING_USER, new Gson().toJson(existingUser)));
            }
        }
    }
}

