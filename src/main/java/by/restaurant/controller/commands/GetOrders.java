package by.restaurant.controller.commands;

import by.restaurant.dao.DAOFactory;
import by.restaurant.dao.DAOFactory.Factories;
import by.restaurant.entity.Order;
import by.restaurant.entity.OrderManagement;

import com.google.gson.Gson;

import java.util.List;
import java.util.Objects;

/**
 * Receives user id from the client
 * defines available operations and
 * available orders, then
 * sends list of {@link by.restaurant.entity.Order} objects
 * and list of {@link by.restaurant.entity.OrderManagement} objects
 * for this user and his role in format json,
 * or sends message that orders list is empty.
 * Overrides {@link FrontCommand#action()}.
 */
public class GetOrders extends FrontCommand {
    private static final String NAME_OPERATIONS_LIST = "\"buttons\": %s }";
    private static final String NAME_ORDERS_LIST = "{\"ordersList\": %s, ";
    private static final String EMPTY_ORDERS_MESSAGE = "{\"ordersList\": \"null\"}";
    private static final String NAME_CLIENT_ROLE = "client";

    @Override
    public void action() {
        DAOFactory mysqlFactory = Objects.requireNonNull(DAOFactory.getDAOFactory(Factories.MySQL));

        //get available user operations
        int userID = Integer.parseInt(request.getParameter("userID"));
        List<OrderManagement> operations = mysqlFactory.getOrderManagementDAO().getOperations(userID);
        int[] statusesID = Objects.requireNonNull(operations).stream()
                .mapToInt(OrderManagement::getCurrentStatusID)
                .toArray();

        //get role name
        String roleName = mysqlFactory.getUserRoleDAO().getUserRole(operations.get(0).getRoleID()).getRoleName();
        //get orders list;
        List<Order> orders;
        if (roleName.equals(NAME_CLIENT_ROLE)) {
            //orders data for client
            orders = mysqlFactory.getOrderDAO().getOrderForClient(userID);
        } else {
            //orders data for admin and chef
            orders = mysqlFactory.getOrderDAO().getOrdersByStatus(statusesID);
        }
        if (orders != null) {
            //send orders list and operations list
            responseJson(String.format(NAME_ORDERS_LIST, new Gson().toJson(orders))
                    + String.format(NAME_OPERATIONS_LIST, new Gson().toJson(operations)));
        } else {
            //if orders list is empty
            responseJson(EMPTY_ORDERS_MESSAGE);
        }
    }
}
