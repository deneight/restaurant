package by.restaurant.controller.commands;

import by.restaurant.dao.DAOFactory;
import by.restaurant.dao.DAOFactory.Factories;
import by.restaurant.dao.interfaces.DAOOrder;
import by.restaurant.dao.interfaces.DAOOrderInfo;

import by.restaurant.utilities.RequestsHandlerWithLogging;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Objects;

/**
 * Receives user id, order id and new status id for this order
 * from the client, changes order status, create new log about this operation,
 * does forwarding to the {@link by.restaurant.controller.commands.GetOrders} command
 * with user id data.
 * Overrides {@link FrontCommand#action()}.
 */
public class EditOrderStatus extends FrontCommand {
    private static final Logger logger = LogManager.getLogger(EditOrderStatus.class);

    @Override
    public void action() {
        DAOFactory mysqlFactory = Objects.requireNonNull(DAOFactory.getDAOFactory(Factories.MySQL));
        DAOOrderInfo log = mysqlFactory.getOrderLoggingDAO();
        DAOOrder order = mysqlFactory.getOrderDAO();

        int[] orderIDStatusID = RequestsHandlerWithLogging.readJSONData(int[].class, request, logger);
        order.changeStatus(orderIDStatusID);

        //add log
        int userID = Integer.parseInt(request.getParameter("userID"));
        log.addLog(Objects.requireNonNull(orderIDStatusID)[0], orderIDStatusID[1], userID);

        forward("GetOrders&userID=" + userID);
    }
}
