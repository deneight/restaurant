package by.restaurant.controller.commands;

import by.restaurant.dao.DAOFactory;
import by.restaurant.dao.DAOFactory.Factories;
import by.restaurant.utilities.RequestsHandlerWithLogging;
import com.google.gson.Gson;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Objects;

/**
 * Receives order id from the client
 * and sends list of {@link by.restaurant.entity.OrderItem} objects
 * a this order in format json.
 * Overrides {@link FrontCommand#action()}.
 */
public class GetOrderItems extends FrontCommand {
    private static final String NAME_ORDER_ITEMS_LIST = "{\"orderItems\": %s }";
    private static final Logger logger = LogManager.getLogger(GetOrderItems.class);

    @Override
    public void action() {
        int orderID = RequestsHandlerWithLogging.readJSONData(int.class, request, logger);

        responseJson(String.format(NAME_ORDER_ITEMS_LIST, new Gson().toJson(Objects.requireNonNull(DAOFactory
                .getDAOFactory(Factories.MySQL))
                .getOrderItemsDAO()
                .getOrderItems(orderID))));
    }
}
