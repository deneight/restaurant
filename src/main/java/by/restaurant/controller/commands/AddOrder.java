package by.restaurant.controller.commands;

import by.restaurant.dao.DAOFactory;
import by.restaurant.dao.DAOFactory.Factories;
import by.restaurant.dao.interfaces.DAOItemTypes;
import by.restaurant.dao.interfaces.DAOOrder;
import by.restaurant.dao.interfaces.DAOOrderInfo;
import by.restaurant.dao.interfaces.DAOOrderItems;
import by.restaurant.entity.ItemType;
import by.restaurant.entity.OrderItem;
import by.restaurant.utilities.RequestsHandlerWithLogging;
import com.google.gson.reflect.TypeToken;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Objects;

/**
 * Receives user id
 * and list of {@link by.restaurant.entity.ItemType} objects
 * from the client, calculates sum all item types, create new order adding this sum
 * and list of {@link by.restaurant.entity.ItemType} objects to this order,
 * then adds new log about this operation.
 * does forwarding to the {@link by.restaurant.controller.commands.GetOrders} command
 * with user id data.
 * Overrides {@link FrontCommand#action()}.
 */
public class AddOrder extends FrontCommand {
    private static final String NAME_ORDER_STATUS_AT_CREATE = "statusAccepted";
    private static final Logger logger = LogManager.getLogger(AddOrder.class);

    @Override
    public void action() {
        DAOFactory mysqlFactory = Objects.requireNonNull(DAOFactory.getDAOFactory(Factories.MySQL));
        DAOItemTypes itemType = mysqlFactory.getItemTypesDAO();
        DAOOrder newOrder = mysqlFactory.getOrderDAO();
        DAOOrderItems orderItems = mysqlFactory.getOrderItemsDAO();
        DAOOrderInfo log = mysqlFactory.getOrderLoggingDAO();

        //read order items
        Type orderItemsList = new TypeToken<List<OrderItem>>() { }
        .getType();
        List<OrderItem> currentOrderItems = RequestsHandlerWithLogging.readJSONData(orderItemsList, request, logger);
        //get cost of each item for checking and calculate order sum
        double sum = 0;
        if (currentOrderItems != null && !currentOrderItems.isEmpty()) {
            int[] listTypeID = currentOrderItems.stream()
                    .mapToInt(OrderItem::getItemTypeID)
                    .toArray();
            List<ItemType> itemTypes = Objects.requireNonNull(itemType.getTypesData(listTypeID));
            for (int i = 0; i < itemTypes.size(); i++) {
                sum += (double) currentOrderItems.get(i).getItemTypesQuantity() * itemTypes.get(i).getItemTypePrice();
            }
        }
        //add new order and order items
        int userID = Integer.parseInt(request.getParameter("userID"));
        if (sum > 0) {
            int orderID = newOrder.addOrder(sum, userID);
            orderItems.addOrderItem(orderID, currentOrderItems);
            //add log
            int statusID = mysqlFactory.getOrderStatusDAO().getOrderStatus(NAME_ORDER_STATUS_AT_CREATE).getStatusID();
            log.addLog(orderID, statusID, userID);
        }
        forward("GetOrders&userID=" + userID);
    }
}
